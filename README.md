Very Rough - only one implementation done

- The search service should probably use a ProviderSearchFactory to resolve which providers can answer the query
- The parser tests (and parser) could be broken up into further components to parse the vairous aspects of the race and markets separately, making it easier to extend/change if the incoming format changes
- This technique allows you to easily onboard new formats to parse and has obvious extension points to bolt on various transports - like receiving files from FTP/SMB/SFTP etc