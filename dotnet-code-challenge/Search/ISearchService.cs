﻿using System.Collections.Generic;
using dotnet_code_challenge.Model.Racing;

namespace dotnet_code_challenge.Search
{
    public interface ISearchService
    {
        IEnumerable<Race> Search(RaceSearchCriteria searchCriteria);
    }
}
