﻿using dotnet_code_challenge.Model;

namespace dotnet_code_challenge.Search
{
    public class RaceSearchCriteria
    {
        public RaceSearchCriteria(string provider)
        {
            Provider = provider;
        }

        public string Provider { get; }
        public Enums.Markets Market { get; set; }
    }
}
