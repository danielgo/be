﻿using System.Collections.Generic;
using System.Linq;
using dotnet_code_challenge.Model.Racing;
using dotnet_code_challenge.Providers;

namespace dotnet_code_challenge.Search
{
    public class SearchService : ISearchService
    {
        private readonly IProviderSearch[] _providerSearches;

        public SearchService(IEnumerable<IProviderSearch> providerSearches)
        {
            _providerSearches = providerSearches.ToArray();
        }

        public IEnumerable<Race> Search(RaceSearchCriteria searchCriteria)
        {
            return _providerSearches
                .Where(p => p.ProviderName == searchCriteria.Provider)
                .SelectMany(p => p.Search(searchCriteria));
        }
    }
}
