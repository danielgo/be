﻿using System;
using System.Collections.Generic;
using System.Text;

namespace dotnet_code_challenge.Model.Racing
{
    public class Price : IComparable
    {
        public uint Value { get; set; }

        public Enums.Markets Market { get; set; }

        public int CompareTo(object obj)
        {
            return Value.CompareTo(obj);
        }
    }
}
