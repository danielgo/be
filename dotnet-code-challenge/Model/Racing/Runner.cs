﻿using System.Collections.Generic;

namespace dotnet_code_challenge.Model.Racing
{
    public class Runner
    {
        public Runner()
        {
            Prices = new List<Price>();
        }

        public string Number { get; set; }
        public string Name { get; set; }

        public List<Price> Prices { get; set; }
    }
}
