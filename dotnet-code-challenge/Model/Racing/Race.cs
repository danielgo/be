﻿using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;

namespace dotnet_code_challenge.Model.Racing
{
    public class Race
    {
        public Race(string externalRaceId, string meetDescription, int number)
        {
            ExternalRaceId = externalRaceId;
            MeetDescription = meetDescription;
            Number = number;
            Runners = new List<Runner>();            
        }

        public void AddRunner(Runner runner)
        {
            Runners.Add(runner);
        }
        

        public string ExternalRaceId { get; }

        public string MeetDescription { get; }

        public int Number { get; }

        public List<Runner> Runners { get; }
    }
}
