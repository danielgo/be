﻿using System;
using System.Collections.Generic;
using System.Linq;
using dotnet_code_challenge.Model;
using dotnet_code_challenge.Providers;
using dotnet_code_challenge.Providers.Caufield;
using dotnet_code_challenge.Providers.Wolverhampton;
using dotnet_code_challenge.Search;
using Unity;

namespace dotnet_code_challenge
{
    public class Program
    {
        static void Main(string[] args)
        {
            //HOOK THE DI LOADER UP HERE PROPERLY
            var container = new UnityContainer();
            container.RegisterType<CaufieldRaceParser>();
            container.RegisterType<WolverhamptonRaceParser>();
            container.RegisterType<IProviderSearch, CaufieldProviderSearch>("Caufield");
            container.RegisterType<IProviderSearch, WolverhamptonProviderSearch>("Wolverhampton");
            container.RegisterType<ISearchService, SearchService>();


            //IN LEIU OF IMPLEMENTING THE COMMANDLINE PARSER OPTIONS FILE, HARDCODE FOR TESTING/DEMONSTRATION
            var searchService = container.Resolve<ISearchService>();

            var races = searchService.Search(new RaceSearchCriteria("Caufield"));
            foreach (var race in races)
            {
                Console.WriteLine(string.Format($"=== Field for Race {race.Number} ==="));
                Console.WriteLine("FIXED WIN MARKET");
                Console.WriteLine("Number\tName\tOdds");

                foreach (var runner in
                    race.Runners.Select(r =>
                        new
                        {
                            r.Number,
                            r.Name,
                            Price = r.Prices.Single(p => p.Market == Enums.Markets.FixedWin).Value

                        }).OrderBy(r => r.Price))


                {
                    Console.WriteLine(
                        $"{runner.Number}\t{runner.Name}\t{(decimal)runner.Price/100}");
                }
            }

            Console.ReadLine();

        }
    }
}
