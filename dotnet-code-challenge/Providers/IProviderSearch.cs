﻿using System.Collections.Generic;
using dotnet_code_challenge.Model.Racing;
using dotnet_code_challenge.Search;

namespace dotnet_code_challenge.Providers
{
    public interface IProviderSearch
    {
        string ProviderName { get;  }
        IEnumerable<Race> Search(RaceSearchCriteria searchCriteria);
    }
}
