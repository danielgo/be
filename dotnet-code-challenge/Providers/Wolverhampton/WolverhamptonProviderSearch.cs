﻿using System;
using System.Collections.Generic;
using System.Text;
using dotnet_code_challenge.Model.Racing;
using dotnet_code_challenge.Search;

namespace dotnet_code_challenge.Providers.Wolverhampton
{
    public class WolverhamptonProviderSearch : IProviderSearch
    {
        private readonly WolverhamptonRaceParser _parser;

        public WolverhamptonProviderSearch(WolverhamptonRaceParser parser)
        {
            _parser = parser;
        }

        public string ProviderName => "Wolverhampton";
        public IEnumerable<Race> Search(RaceSearchCriteria searchCriteria)
        {
            throw new NotImplementedException();
        }
    }
}
