﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml.Linq;
using dotnet_code_challenge.Model.Racing;
using dotnet_code_challenge.Search;

namespace dotnet_code_challenge.Providers.Caufield
{
    public class CaufieldProviderSearch : IProviderSearch
    {
        private readonly CaufieldRaceParser _parser;
        private string DataFolder = ".\\FeedData";
        private string FileGlob = "Caulfield*.xml";
        public CaufieldProviderSearch(CaufieldRaceParser parser)
        {
            _parser = parser;
        }

        public string ProviderName => "Caufield";
        public IEnumerable<Race> Search(RaceSearchCriteria searchCriteria)
        {
            return LoadAndParse().SelectMany(x => _parser.Parse(x));
        }

        private IEnumerable<XDocument> LoadAndParse()
        {
            return Directory.EnumerateFiles(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), DataFolder), FileGlob)
                .Select(XDocument.Load);
        }
    }
}
