﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using dotnet_code_challenge.Model;
using dotnet_code_challenge.Model.Racing;

namespace dotnet_code_challenge.Providers.Caufield
{
    public class CaufieldRaceParser
    {

        public virtual IEnumerable<Race> Parse(XDocument caufieldData)
        {
            foreach (var race in caufieldData.Root.Descendants("race"))
            {
                var result = new Race(race.Attribute("id").Value, "", int.Parse(race.Attribute("number").Value));
                foreach (var horse in race
                    .Elements("horses")
                        .Descendants("horse"))
                {
                    var runner = new Runner
                    {
                        Name = horse.Attribute("name").Value,
                        Number =  horse.Element("number").Value
                    };
                    foreach (var market in race
                        .Elements("prices")
                            .Descendants("price"))
                    {
                        var price = market.Descendants("horse")
                            .Single(h => h.Attribute("number").Value == runner.Number).Attribute("Price").Value;
                        runner.Prices.Add(new Price
                        {
                            Market = ResolveMarket(market.Element("priceType").Value),
                            Value = ResolveValue(price)

                        });

                    }

                    result.AddRunner(runner);
                }

                yield return result;
            }
            
        }

        private uint ResolveValue(string value)
        {
            //Assuming dollars - parse and convert to cents
            return (uint) (decimal.Parse(value) * 100);
        }

        private static Enums.Markets ResolveMarket(string value)
        {
            switch (value)
            {
                case "WinFixedOdds":
                    return Enums.Markets.FixedWin;
            }

            throw new ArgumentException("unknown market");
        }
        
    }
}
