﻿using System.Collections.Generic;
using System.Linq;
using dotnet_code_challenge.Model.Racing;
using dotnet_code_challenge.Providers;
using dotnet_code_challenge.Search;
using Moq;
using NUnit.Framework;

namespace dotnet_code_challenge.Test.Search
{
    [TestFixture]
    public class SearchServiceTest
    {
        [Test]
        public void ShouldGetSingleRaceFromSingleProvider()
        {
            var providerSearch = new Mock<IProviderSearch>();
            providerSearch.Setup(p => p.Search(It.IsAny<RaceSearchCriteria>())).Returns(new[] {new Race("1234", "MockMeet", 1)});
            providerSearch.Setup(p => p.ProviderName).Returns("McRaceTrack");

            var searchService = new SearchService(new[] {providerSearch.Object});
            var races = searchService.Search(new RaceSearchCriteria("McRaceTrack"));

            Assert.IsNotNull(races);
            Assert.That(races.Count() == 1);
        }

        [Test]
        public void ShouldGetMultipleRacesFromSingleProvider()
        {
            var providerSearch = new Mock<IProviderSearch>();
            providerSearch.Setup(p => p.Search(It.IsAny<RaceSearchCriteria>())).Returns(new[] { new Race("1234", "MockMeet", 1) , new Race("5678", "MockMeet", 2) });
            providerSearch.Setup(p => p.ProviderName).Returns("Doomben");

            var searchService = new SearchService(new[] { providerSearch.Object });
            var races = searchService.Search(new RaceSearchCriteria("Doomben")).ToList();
            
            Assert.That(races.Count == 2);
            Assert.That(races[0].Number == 1);
            Assert.That(races[1].Number == 2);
        }

        [Test]
        public void ShouldGetNoRaces()
        {
            var providerSearch = new Mock<IProviderSearch>();
            providerSearch.Setup(p => p.Search(It.IsAny<RaceSearchCriteria>())).Returns(Enumerable.Empty<Race>());
            providerSearch.Setup(p => p.ProviderName).Returns("DaptoDogs");

            var searchService = new SearchService(new[] { providerSearch.Object });
            var races = searchService.Search(new RaceSearchCriteria("DaptoDogs")).ToList();

            Assert.That(races.Count == 0);
        }

        [Test]
        public void ShouldGetRaceFromSuppliedProvider()
        {
            var caufield = new Mock<IProviderSearch>();
            caufield.Setup(p => p.Search(It.IsAny<RaceSearchCriteria>())).Returns(new[] { new Race("1234", "MockMeet", 1) });
            caufield.Setup(p => p.ProviderName).Returns("Caufield");
            var wolver = new Mock<IProviderSearch>();
            wolver.Setup(p => p.Search(It.IsAny<RaceSearchCriteria>())).Returns(new[] { new Race("5689", "Wolver Races", 1) });
            wolver.Setup(p => p.ProviderName).Returns("Wolverhampton");

            var searchService = new SearchService(new[] {caufield.Object, wolver.Object});
            var races = searchService.Search(new RaceSearchCriteria("Wolverhampton") ).ToList();

            Assert.That(races.Count == 1);
            Assert.That(races[0].ExternalRaceId == "5689");
            Assert.That(races[0].MeetDescription == "Wolver Races");
            Assert.That(races[0].Number == 1);

        }        
    }
}
